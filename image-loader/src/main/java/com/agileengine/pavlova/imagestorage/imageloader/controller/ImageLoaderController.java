package com.agileengine.pavlova.imagestorage.imageloader.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController("/images")
public class ImageLoaderController {

    @GetMapping
    public ResponseEntity loadImages(){
        return ResponseEntity.ok().build();
    }

    @GetMapping("?page={page}")
    public ResponseEntity loadImageDetails(@PathVariable("page") String page){
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity loadImagesFromPage(@PathVariable("id") String id){
        return ResponseEntity.ok().build();
    }
}