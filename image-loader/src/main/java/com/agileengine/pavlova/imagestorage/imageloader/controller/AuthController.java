package com.agileengine.pavlova.imagestorage.imageloader.controller;

import com.agileengine.pavlova.imagestorage.imageloader.body.AuthBody;
import com.agileengine.pavlova.imagestorage.imageloader.service.AuthService;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController("/auth")
public class AuthController {
    public static final String TOKEN = "token";
    @Autowired
    private AuthService authService;

    @PostMapping
    public ResponseEntity auth(@RequestBody AuthBody authBody) throws IOException, JSONException {
        return ResponseEntity
                .status(HttpStatus.ACCEPTED)
                .body(authService.auth(authBody).getString(TOKEN));
    }
}
