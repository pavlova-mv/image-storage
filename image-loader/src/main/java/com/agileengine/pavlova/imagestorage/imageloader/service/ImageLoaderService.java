package com.agileengine.pavlova.imagestorage.imageloader.service;

import com.agileengine.pavlova.imagestorage.imageloader.bean.Image;
import com.agileengine.pavlova.imagestorage.imageloader.repository.ImageLoaderRepository;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Service
public class ImageLoaderService {
    public static final String URL = "http://interview.agileengine.com/images";
    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String JUST_TEMPORARY = "Bearer eba5bd43778ff4f27154de46bad96530375d56ea";
    @Autowired
    private ImageLoaderRepository imageLoaderRepository;

    public JSONObject loadImages() throws IOException, JSONException {
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(URL);
        request.setHeader(AUTHORIZATION_HEADER, JUST_TEMPORARY);
        HttpResponse response = httpClient.execute(request);
        JSONObject images = new JSONObject(IOUtils
                .toString(response
                        .getEntity()
                        .getContent(), StandardCharsets.UTF_8));
        imageLoaderRepository.saveCachedImages(convertJsonToList(images));
        return images;
    }

    private List<Image> convertJsonToList(JSONObject images) {
        return new ArrayList<>();
    }
}
