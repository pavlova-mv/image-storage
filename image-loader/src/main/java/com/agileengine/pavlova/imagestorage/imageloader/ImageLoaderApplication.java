package com.agileengine.pavlova.imagestorage.imageloader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImageLoaderApplication {
    public static void main(String[] args) {
        SpringApplication.run(ImageLoaderApplication.class, args);
    }
}
