package com.agileengine.pavlova.imagestorage.imageloader.repository;

import com.agileengine.pavlova.imagestorage.imageloader.bean.Image;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImageLoaderRepository {
    void saveCachedImages(List<Image> images);
}
