package com.agileengine.pavlova.imagestorage.imageloader.body;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthBody {
    private String apiKey;
}
