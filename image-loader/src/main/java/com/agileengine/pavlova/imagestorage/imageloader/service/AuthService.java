package com.agileengine.pavlova.imagestorage.imageloader.service;

import com.agileengine.pavlova.imagestorage.imageloader.body.AuthBody;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Service
public class AuthService {
    public static final String URL = "http://interview.agileengine.com/auth";
    public static final String CONTENT_TYPE = "content-type";
    public static final String CONTENT_TYPE_VALUE = "application/json";
    private static final String ACCESS_TOKEN_COOKIE_NAME = "Access_cookie";
    public static final String TOKEN = "token";

    public JSONObject auth(AuthBody authBody) throws IOException, JSONException {
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost request = new HttpPost(URL);
        StringEntity params = new StringEntity("{\"apiKey\": \"" + authBody.getApiKey() + "\"}");
        request.addHeader(CONTENT_TYPE, CONTENT_TYPE_VALUE);
        request.setEntity(params);
        HttpResponse response = httpClient.execute(request);
        JSONObject result = new JSONObject(IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8));
        return result;
    }
}
